/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaexercised;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author 2ndyrGroupA
 */
public class JavaExerciseD {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("• Number 1");
        sorting();
        System.out.println("");
        System.out.println("");
        System.out.println("• Number 2");
        shuffling();
        System.out.println("");
        System.out.println("");
        System.out.println("• Number 3");
        numbers();
        System.out.println("");
        System.out.println("");
        System.out.println("•Number 4");
        System.out.println("Question:");
        System.out.println("    Why should we opt for isEmpty() over size?");
        System.out.println("Answer:");
        System.out.println("    The reason why we should use isempty rather that size is because the code is easier to read and understand and the execution is fast.");
        System.out.println("•Number 5");
        System.out.println("Question:");
        System.out.println("    Compare and contrast the classic for loop versus foreach. What are the pros and cons of both sides?");
        System.out.println("Answer:");
        System.out.println("    The foreach loop cannot modify a collection , while a for loop can. The pros of both sides is being flexible in reading data. The problem with this type of writing is that you can't jump out of the midway, and the break command can't be returned from the closure function with the return statement.");
        System.out.println("");
        System.out.println("");
        System.out.println("• Number 6");
        compare();
        System.out.println("");
        System.out.println("");
        System.out.println("• Number 7");
        unique();
        System.out.println("");
        System.out.println("");
        System.out.println("• Number 8");
        countKey();
    }
    
    //Number 1
    public static void sorting() {
        List<String> words = new ArrayList<String>();
        words.add("Daryll");
        words.add("Jurick");
        words.add("Aldrin");
        words.add("Jeric");
        words.add("April");
        words.add("Rhea");
        words.add("Winabel");
        words.add("Christine");
        words.add("Marjorie");
        System.out.println("Orininal Sequence:");
        System.out.println(words);
        words.sort(null);
        System.out.println("Sorting the names to Ascending order:");
        for (String counter : words) {
            System.out.println(counter);
        }
    }
    
    //Number 2
    public static void shuffling() {
        List<String> fruits = new ArrayList<String>();
        fruits.add("Guava");
        fruits.add("Lemon");
        fruits.add("Watermelon");
        fruits.add("Orange");
        fruits.add("Guyabano");
        fruits.add("Pandan");
        fruits.add("Pinaapple");
        fruits.add("Apple");
        System.out.println("Original Sequence:");
        System.out.println(fruits);
        Collections.shuffle(fruits);
        System.out.println("Shuffling the fruits:");
        for (String count : fruits) {
            System.out.println(count);
        }

    }
    
    //Number 3
    public static void numbers() {
        List<Integer> numbers = new ArrayList<Integer>();
        numbers.add(3);
        numbers.add(8);
        numbers.add(23);
        numbers.add(91);
        numbers.add(6);
        numbers.add(1);
        System.out.println("Original Sequence:");
        System.out.println(numbers);
        System.out.println("Moves the minimum value in front:");
        int min = 0;
        for (int i = 1; i < numbers.size(); i++) {
            if (numbers.get(i) < numbers.get(min)) {
                min = i;
            }
        }
        numbers.add(0, numbers.remove(min));
        System.out.println(numbers);
    }
    
    //Number 6
    public static void compare() {
        List<String> nametag = new ArrayList<String>();
        nametag.add("Car");
        nametag.add("Motor");
        nametag.add("Plane");
        nametag.add("Bicycle");
        nametag.add("Jet");
        System.out.println("Set 1:");
        System.out.println(nametag);
        List<String> nametag2 = new ArrayList<String>();
        nametag2.add("Submarine");
        nametag2.add("Helicopter");
        nametag2.add("Car");
        nametag2.add("Boat");
        nametag2.add("Motor");
        System.out.println("Set 2:");
        System.out.println(nametag2);
        System.out.println("Common Values:");
        nametag.retainAll(nametag2);
        System.out.println(nametag);
    }
    
    //Number 7
    public static void unique() {
        ArrayList<Integer> num1 = new ArrayList<Integer>();
        num1.add(1);
        num1.add(31);
        num1.add(24);
        num1.add(33);
        num1.add(12);
        num1.add(27);
        System.out.println("Set 1:");
        System.out.println(num1);
        List<Integer> num2 = new ArrayList<Integer>();
        num2.add(3);
        num2.add(27);
        num2.add(12);
        num2.add(11);
        num2.add(1);
        num2.add(8);
        System.out.println("Set 2:");
        System.out.println(num2);
        System.out.println("Unique Values:");
        Set<Integer> union = new HashSet<Integer>(num1);
        union.removeAll(num2);
        Set<Integer> intersection = new HashSet<Integer>(num2);
        intersection.removeAll(num1);
        union.addAll(intersection);
        System.out.println(union);
    }
    
    //Number 8
    public static void countKey() {
        HashMap<String, Integer> people = new HashMap<String, Integer>();
        people.put("John", 32);
        people.put("Steve", 30);
        people.put("Angie", 33);
        people.put("Daryll", 33);
        people.put("Steve32", 30);
        people.put("Dario", 33);
        people.put("Steve1", 30);
        int count = 0;
        for (String key : people.keySet()) {
            if(key.startsWith("Steve")){
                count++;
                
            }     
        }
        System.out.println("Output: "+count);
    }
        
    
    
}
